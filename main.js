import React from 'react';
import ReactDOM from 'react-dom';

import normalize from 'normalize.css';
import milligram from 'milligram';

class Test extends React.Component {
    render() {
        return <button className="button button-outline">Outlined Button</button>;
    }
}

ReactDOM.render(
    <Test />,
    document.getElementById('app')
);